/*
Navicat PGSQL Data Transfer

Source Server         : 10.15.74.2-AKADEMIK
Source Server Version : 70407
Source Host           : 10.15.74.2:5432
Source Database       : akademik
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 70407
File Encoding         : 65001

Date: 2018-08-30 13:37:01
*/


-- ----------------------------
-- Table structure for agama
-- ----------------------------
DROP TABLE "public"."agama";
CREATE TABLE "public"."agama" (
"agamaid" int4 DEFAULT nextval('agama_agamaid_seq'::text) NOT NULL,
"agama" varchar
)
WITH OIDS 

;

-- ----------------------------
-- Records of agama
-- ----------------------------
INSERT INTO "public"."agama" VALUES ('1', 'Islam');
INSERT INTO "public"."agama" VALUES ('2', 'Katolik');
INSERT INTO "public"."agama" VALUES ('3', 'Kristen');
INSERT INTO "public"."agama" VALUES ('4', 'Budha');
INSERT INTO "public"."agama" VALUES ('5', 'Hindu');
INSERT INTO "public"."agama" VALUES ('6', null);
INSERT INTO "public"."agama" VALUES ('7', 'Konghuchu');

-- ----------------------------
-- Indexes structure for table agama
-- ----------------------------
CREATE UNIQUE INDEX "agama_agamaid_key" ON "public"."agama" USING btree ("agamaid");
