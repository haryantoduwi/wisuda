/*
Navicat PGSQL Data Transfer

Source Server         : 10.15.74.2-AKADEMIK
Source Server Version : 70407
Source Host           : 10.15.74.2:5432
Source Database       : akademik
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 70407
File Encoding         : 65001

Date: 2018-08-30 13:38:01
*/


-- ----------------------------
-- Table structure for dosen
-- ----------------------------
DROP TABLE "public"."dosen";
CREATE TABLE "public"."dosen" (
"kode" varchar(6) NOT NULL,
"nama" varchar(50),
"gelar1" varchar(25),
"gelar2" varchar(25),
"nodos" varchar(25),
"kodefak" varchar(4),
"goldos" varchar(10),
"jabdos" varchar(25),
"jsks" int2,
"jklas" int2,
"jgabung" int2,
"status" varchar(4),
"kodegol" int2,
"aktif" varchar(25),
"urut" int4,
"nidn" varchar(15),
"jur" varchar(2),
"angket" int4
)
WITH OIDS 

;

-- ----------------------------
-- Records of dosen
-- ----------------------------
INSERT INTO "public"."dosen" VALUES ('0469', 'RATNA JUWIANTY', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '9905000469', null, null);
INSERT INTO "public"."dosen" VALUES ('0593', 'NYEO TONY MARTONO', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '9905000593', null, null);
INSERT INTO "public"."dosen" VALUES ('0815', 'SUMPENO WIDIANTO', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '9905000815', null, null);
INSERT INTO "public"."dosen" VALUES ('2571', 'SYAROF AL KHOIRI', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '9905002571', null, null);
INSERT INTO "public"."dosen" VALUES ('3361', 'ETIEK FEBRIYANTI', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '9905003361', null, null);
INSERT INTO "public"."dosen" VALUES ('4196', 'NURANI DEWI', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '9905004196', null, null);
INSERT INTO "public"."dosen" VALUES ('4543', 'ICHWANUS SOLICHIN', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '9905004543', null, null);
INSERT INTO "public"."dosen" VALUES ('5053', 'WIDI ERMA ARWANTI', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '9905005053', null, null);
INSERT INTO "public"."dosen" VALUES ('7401', 'APO WIJONO', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '0506117401', null, null);
INSERT INTO "public"."dosen" VALUES ('7903', 'DEWI INDRASWATI', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '0514057903', null, null);
INSERT INTO "public"."dosen" VALUES ('8002', 'ARI YOGA RISYANA', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '0510078002', null, null);
INSERT INTO "public"."dosen" VALUES ('8201', 'PUJIYANTO', null, null, null, null, null, null, null, null, null, null, null, 'Keluar', null, '0519048201', null, null);
INSERT INTO "public"."dosen" VALUES ('AAPS', 'Anak Agung Putu Susastriawan', '', 'S.T., M.Tech.', '04 1077 589 E', 'FTI', 'III/c', 'Lektor ', null, null, null, 'DPY', null, null, null, '0508107701', '33', '200');
INSERT INTO "public"."dosen" VALUES ('AARI', 'Arham Arifuddin', '', 'S.Kom.', null, null, null, '', null, null, null, 'DPY', null, 'Keluar', null, '0503058102', '05', null);
INSERT INTO "public"."dosen" VALUES ('ABDUL', 'Abdulrahman', '', 'M.Si.', null, null, null, null, '9', '4', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ABUD', 'Arifah Budhyati Mz', 'Dra.', '', '94 0956 497 E', null, 'III/b', 'Asisten Ahli', '18', '9', '8', 'DPY', null, null, null, '0530095601', '02', '100');
INSERT INTO "public"."dosen" VALUES ('ADAR', 'Arief Darmawan', 'Ir.', null, null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ADJOK', 'Adi Djoko Guritno', 'DR. Ir.', 'MSIE', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ADUN', 'Agus Duniawan', '', 'S.T., M.Eng', '85 1156 266 E', null, 'III/b', 'Asisten Ahli ', '22', '11', '0', 'DPY', null, null, null, '0511115601', '33', '100');
INSERT INTO "public"."dosen" VALUES ('ADWA', 'Angge Dhevi Warisaura', '', 'S.T. M.Eng', '14 1290 702 E', null, 'III/a', '', null, null, null, 'DPY', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('AEDD', 'Alpha Eddy Tantowi', 'Ir.', 'M.Sc.,Ph.D', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('AFAT', 'Achmad Arifin', '', 'S.Pd, M.Eng', null, null, 'III/b', null, '0', '0', '0', 'DPY', '0', 'Keluar', null, '0014037105', null, null);
INSERT INTO "public"."dosen" VALUES ('AGUR', 'Adi Guritno', 'Dr.Ir.', 'MSIE', null, null, null, null, '0', '0', '0', 'DPY', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('AHAD', 'Abdul Hadi Kadarusno', '', 'SKM.', null, 'MIPA', 'IV/a', 'Lektor', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('AHAM', 'Amir Hamzah', 'Dr. Ir.', 'MT.', '87 0563 319 E', null, 'IV/a', 'Lektor Kepala ', '20', '7', '0', 'DPY', '4', null, null, '0505056302', '05', '400');
INSERT INTO "public"."dosen" VALUES ('AHAR', 'A. HARDJONO', 'Ir.', '', null, null, null, null, '6', '3', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('AJOK', 'Agus Djoko Santosa', 'Prof.', 'SU.', null, 'MIPA', null, 'Guru Besar', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('AKAD', 'ABDUL KADIR', 'Ir.', 'MT.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('AKUN', 'Achmad Kuntadi M.', 'Ir.', '', null, null, 'III/d', 'Lektor', '0', '0', '0', 'DPY', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ALFIH', 'Afiati Handayu Diyah Fitriyani', '', 'S.Pd., M.Pd.', null, null, 'III/b', null, null, null, null, 'Luar', null, null, null, '2012078501', null, null);
INSERT INTO "public"."dosen" VALUES ('AMUA', 'Alif Mu`arifah', 'Dra. Hj.', 'S.Psi,M.Si', null, null, null, 'Lektor', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('AMUS', 'Agus Mustofa', 'Ir.', 'M.Sc.', null, null, null, 'Lektor Kepala', '9', '3', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ANDI', 'Andi Rahadian W', '', 'M.Sc.', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ANDRE', 'Andrean Emaputra', null, 'S.T., M.Sc.', '15 0388 729 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0531038802', '02', null);
INSERT INTO "public"."dosen" VALUES ('ANGG', 'Anggodo', 'Ir.', null, null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ANOR', 'Arie Noor Rakhman', null, 'S.T., M.T.', '08 0576 648 E', 'FTM', 'III/b', 'Asisten Ahli ', null, null, null, 'DPY', null, null, null, '0529057602', '10', '150');
INSERT INTO "public"."dosen" VALUES ('ANPU', 'Ani Purwanti', '', 'S.T., M.Eng', '04 0481 592 E', 'FTI', 'III/c', 'Lektor ', null, null, null, 'DPY', null, null, '8', '0502048101', '01', '200');
INSERT INTO "public"."dosen" VALUES ('APRA', 'Aji Pranoto', '', 'S.Pd., M.Pd.', '19710611 200501 1 003', null, 'III/b', 'Lektor', null, null, null, 'DPK', null, null, null, '0011067101', '33', '200');
INSERT INTO "public"."dosen" VALUES ('APUR', 'Adi Purwanto', 'Ir.', 'MT.', '88 1156 360 E', null, 'III/c', 'Asisten Ahli', '28', '14', '0', 'DPY', '3', null, null, '0505115601', '03', '150');
INSERT INTO "public"."dosen" VALUES ('ARTS', 'ARI TATA SUSILA', null, null, null, null, null, null, null, null, null, null, null, null, null, '0514037102', '03', null);
INSERT INTO "public"."dosen" VALUES ('ARUK', 'Endang Rukmini', 'Ir.', '', null, null, null, 'Lektor', '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ASHAR', 'Ashari', 'Drs.', 'MT.', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ASTAN', 'Nur Widi Astanto Agus Triheriyadi', '', 'S.T., M.T.', '15 0879 730 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0521087901', '10', null);
INSERT INTO "public"."dosen" VALUES ('ASUS', 'Adhi Susanto', 'Prof.', '', null, null, null, 'Guru Besar', '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('AWIB', 'Arif Wibisono', 'Ir.', 'MT.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('BARM', 'Subarmono', 'Ir.', 'MT.', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('BDHS', 'Boby Dwi Herquariyanto Supomo', null, 'S.T., M.T.', null, null, null, null, null, null, null, 'DPY', null, null, null, null, '10', null);
INSERT INTO "public"."dosen" VALUES ('BEKO', 'Bernadetta Eko Putranti', 'Dra.', 'M.Hum', '92 0865 447 E', null, 'III/b', 'Asisten Ahli', '12', '6', '0', 'DPY', null, null, null, '0520086501', '03', '100');
INSERT INTO "public"."dosen" VALUES ('BFIR', 'Beny Firman', '', 'S.T., M.Eng.', '13 0786 649 E', null, 'III/b', 'Asisten Ahli ', null, null, null, 'DPY', null, null, null, '0505078601', '04', '150');
INSERT INTO "public"."dosen" VALUES ('BKUS', 'Bambang Kusmartono', '', 'ST., MT.', '84 0360 234 E', null, 'III/c', 'Lektor', '13', '6', '0', 'DPY', '2', null, null, '0503036001', '01', '200');
INSERT INTO "public"."dosen" VALUES ('BMUR', 'Bardi Murahman', 'Ir.', 'SU. Ph.D', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('BPRAS', 'Bambang Prastowo', 'Drs.', 'M.Sc.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('BSAN', 'Budi Santoso', 'Dr.', 'M.Eng', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('BSET', 'Budi Setyanto', 'Ir.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('BSKN', 'Basikin', null, 'M.Phil.,M.Ed', null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('BSOE', 'Bambang Soedijono Wiriaatmadja', 'Prof. Dr.', '', null, null, 'IV/c', 'Guru Besar', '2', '1', '0', 'DPY', null, 'Keluar', '1', null, null, null);
INSERT INTO "public"."dosen" VALUES ('BSUD', 'Bambang Sudiyana', '', '', null, 'FTI', null, null, null, null, null, null, null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('BSUP', 'Bambang Supriyanto', 'Drs.', 'S.Si.', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('BTRIS', 'Beni Tri Sasongko', null, 'S.T., M.Eng.', '15 0586 735 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0504058601', '03', null);
INSERT INTO "public"."dosen" VALUES ('BWAH', 'Bambang Wahyu Sidharta', 'Ir.', 'M.Eng', '04 0260 588 E', '', 'III/b', 'Asisten Ahli', null, null, null, 'DPY', null, null, null, '0503026002', '33', '150');
INSERT INTO "public"."dosen" VALUES ('CEYU', 'Cesaria Eka Yulianti', '', 'S.H., S.T., M.T.', null, null, 'III/c', 'Asisten Ahli', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('CIND', 'Cyrilla Indri Parwati', '', 'ST., M.T.', '01 0374 567 E', null, 'III/d', 'Lektor', '6', '3', '0', 'DPY', null, null, null, '0518037401', '02', '300');
INSERT INTO "public"."dosen" VALUES ('CISW', 'Catur Iswahyudi', '', 'S.Kom, S.E, M.Cs, MTA', '93 0673 467 E', 'FTI', 'III/b', 'Lektor', null, null, null, 'DPY', null, null, '6', '0519067301', '35', '100');
INSERT INTO "public"."dosen" VALUES ('CWIS', 'Christanto Wismanegara', 'Drs.', 'M.Hum.', null, null, null, null, '2', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('DAND', 'Dina Andayati', 'Dra.', 'MT.', '88 0163 352 E', null, 'III/c', 'Lektor', '9', '3', '0', 'DPY', null, null, '7', '0509016301', '35', '300');
INSERT INTO "public"."dosen" VALUES ('DARM', 'Sudarmanto', 'Ir.', 'MT', null, null, 'III/b', 'Asisten Ahli', '27', '11', '0', 'DPY', '2', 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('DARS', 'Sudarsono', 'Dr.Ir.', 'MT.', '88 0255 359 E', null, 'IV/c', 'Lektor Kepala ', '19', '8', '0', 'DPY', '4', null, '4', '0519025501', '03', '700');
INSERT INTO "public"."dosen" VALUES ('DAWIL', 'Danis Agoes Wiloso', null, 'S.T., M.T.', '16 0869 767 E', null, null, 'Asisten Ahli', null, null, null, 'DPY', null, null, null, '0529086901', '10', null);
INSERT INTO "public"."dosen" VALUES ('DEHE', 'Deni Herdian', 'S.Kom', null, null, null, null, null, null, null, null, null, null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('DGAB', 'Dosen Gabungan', '', '', null, null, null, null, '0', '0', '0', 'TIM', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('DHAR', 'Denny Hardiyanto', null, 'S.T., M.Eng.', null, null, null, null, null, null, null, 'DPY', null, null, null, '0504048901', '04', null);
INSERT INTO "public"."dosen" VALUES ('DIND', 'Dwi Indah Purnamawati', 'Ir.', 'M.Si.', '91 0659 413 E', null, 'IV/b', 'Lektor Kepala', '22', '11', '4', 'DPY', null, null, '6', '0521065902', '10', '400');
INSERT INTO "public"."dosen" VALUES ('DKIS', 'Desi Kiswiranti', '', 'S.Si, M.Sc', '15 1285 738 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0502128501', '10', null);
INSERT INTO "public"."dosen" VALUES ('DKOR', 'Dwi Korita Karnawati', 'DR. Ir.', '', null, null, null, null, '2', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('DKUR', 'Stephanus Danny Kurniawan', null, 'S.T., M.Eng.', '15 1183 728 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0505118301', '03', null);
INSERT INTO "public"."dosen" VALUES ('DMAR', 'Didi Mardijanto,', '', 'S.T.', null, 'FTI', null, null, null, null, null, null, null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('DROS', 'Dedy Rosadi', '', 'M.Si.', null, null, null, null, '0', '0', '0', 'Luar', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('DSET', 'Dwi Setyowatie', 'Dra.', 'MT.', '19590722 199003 2 001', null, 'III/c', 'Lektor ', '12', '4', '0', 'DPK', '1', null, null, '0022075902', '07', '200');
INSERT INTO "public"."dosen" VALUES ('DSIH', 'Diman Sihole', 'Ir.', 'MT.', null, null, 'III/d', 'Lektor', '0', '0', '0', 'DPY', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('DSTAN', 'S. Djalal Tandjung', 'Prof. Dr.', 'M.Sc', null, null, 'IV/e', 'Guru Besar', '10', '5', '0', 'DPY', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('DTAN', 'Dina Tania', '', 'ST.,MT', '13 0582 700 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0518058202', '10', null);
INSERT INTO "public"."dosen" VALUES ('DWAH', 'Dewi Wahyuningtyas', null, 'S.T., M.Eng', '16 0789 752 E', 'FTI', 'III/b', null, null, null, null, 'DPY', null, null, null, '0517078901', '01', null);
INSERT INTO "public"."dosen" VALUES ('DWIN', 'Djoko Wintolo', 'Ir.', 'DEA.', null, null, null, null, '2', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('DYAH', 'Dyah Anggun Sartika', null, 'S.T., M.Eng.', null, null, null, null, null, null, null, 'DPY', null, null, null, '0503029002', '04', null);
INSERT INTO "public"."dosen" VALUES ('DZAKI', 'Nurul Dzakiya', '', 'S.Si, M.Sc', '14 0187 706 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0521018701', '10', null);
INSERT INTO "public"."dosen" VALUES ('EAST', 'Endang Astuti', 'Dra.', 'M.Si.', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ECOP', 'Ermelindo Cofitalan', 'Drs.', 'BA, M.Hum, Lic.Th', null, null, null, null, '12', '6', '8', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('EFAT', 'Erfanti Fatkhiyah', '', 'S.T.,M.Cs.', '00 1273 564 E', 'FTI', 'III/b', 'Asisten Ahli ', null, null, null, 'DPY', null, null, null, '0520127301', '35', '150');
INSERT INTO "public"."dosen" VALUES ('EKSU', 'EKO SUKIYANTO', null, null, null, null, null, null, null, null, null, null, null, null, null, '9905002968', '05', null);
INSERT INTO "public"."dosen" VALUES ('EKSUL', 'Eka Sulistyaningsih', null, 'S.Si., M.Sc.', '19861216 201504 2 002', null, 'III/b', null, null, null, null, 'DPK', null, null, null, '0016128601', '32', null);
INSERT INTO "public"."dosen" VALUES ('EKUM', 'Erna Kumalasari Nurnawati', '', 'ST.,MT.', '96 1071 514 E', null, 'IV/a', 'Lektor Kepala', '9', '3', '0', 'DPY', '2', null, null, '0503017101', '05', '400');
INSERT INTO "public"."dosen" VALUES ('EMAR', 'Ekawati Martyaningsih', '', 'ST.,MT.', null, null, 'III/b', 'Asisten Ahli', '24', '9', '0', 'DPY', '1', 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ESET', 'Emy Setyaningsih', '', 'S.Si.,M.Kom.', '97 1072 526 E', null, 'IV/a', 'Lektor Kepala ', '23', '7', '3', 'DPY', '1', null, '6', '0530107201', '07', '400');
INSERT INTO "public"."dosen" VALUES ('ESETA', 'Ellyawan Setyo Arbintarso', '', 'ST,M.Sc.,Ph.D.', '97 0371 525 E', null, 'IV/a', 'Lektor Kepala', '6', '2', '0', 'DPY', '1', null, null, '0527037101', '03', '400');
INSERT INTO "public"."dosen" VALUES ('ESUS', 'Erma Susanti', '', 'S.Kom., M.Cs.', '06 1282 635 E', null, 'III/b', 'Asisten Ahli', null, null, null, 'DPY', null, null, null, '0531128201', '35', '150');
INSERT INTO "public"."dosen" VALUES ('ESUT', 'Edhy Sutanta', 'Dr.', 'ST.,M.Kom.', '96 0372 515 E', null, 'IV/b', 'Lektor Kepala', '30', '12', '3', 'DPY', '2', null, '6', '0508037201', '05', '550');
INSERT INTO "public"."dosen" VALUES ('ESUW', 'Endy Suwondo', 'DR.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ETHR', 'Erry Thriana Sasongko', '', 'S.T.', '04 0869 577 E', 'FTI', 'III/a', null, null, null, null, 'DPY', null, null, null, '0504086904', '33', null);
INSERT INTO "public"."dosen" VALUES ('ETRI', 'Endang Tri Wahyuni', 'Dr.', '', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('EWID', 'Endang Widuri Asih', '', 'S.T.,M.T.', '95 0969 507 E', null, 'III/d', 'Lektor', '0', '0', '0', 'DPY', '2', null, '8', '0515096901', '02', '300');
INSERT INTO "public"."dosen" VALUES ('FASN', 'Firma Syahrian', null, 'S.Kom., M.Cs.', null, null, null, null, null, null, null, 'DPY', null, null, null, null, '05', null);
INSERT INTO "public"."dosen" VALUES ('FAUZ', 'Fauzun', '', 'ST.,MT.', null, 'FTI', null, 'Asisten Ahli', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('FDAN', 'F Danang Wijaya', '', 'ST.', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('FHAR', 'Fx. Haryanto', 'Drs.', '', null, null, 'III/b', 'Asisten Ahli', '12', '6', '0', 'DPK', '2', 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('FIWEL', 'Fivry Wellda Maulana', '', 'ST., MT.', '12 0182 684 E', 'FTM', 'III/b', 'Asisten Ahli ', null, null, null, 'DPY', null, null, null, '0518018201', '10', '150');
INSERT INTO "public"."dosen" VALUES ('FMOH', 'Feriyanto Mohi', '', 'S.Kom.', null, null, 'III/a', '', null, null, null, 'DPY', null, 'Keluar', null, '0529058301', '05', null);
INSERT INTO "public"."dosen" VALUES ('FRM', 'Faisal RM.', 'Ir. Drs.', 'MSIE', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('FSUK', 'FX. Sukidjo', 'Ir.', 'MT.', null, null, null, 'Lektor', '0', '0', '0', 'DPK', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('FSUS', 'F Soesianto', 'DR.', '', null, null, null, null, '4', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('GABI', 'Miss. Gabi', '', '', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('GAND', 'Ganjar Andaka', 'Ir.', 'Ph.D', '19630307 199403 1 001', null, 'III/a', 'Asisten Ahli ', '0', '0', '0', 'DPK', '1', null, null, '0007036302', '01', '100');
INSERT INTO "public"."dosen" VALUES ('GARM', 'GANA ARDITYA MULIA', null, null, null, null, null, null, null, null, null, null, null, null, null, '9905004516', '05', null);
INSERT INTO "public"."dosen" VALUES ('GFAU', 'Gusti Fauza', '', 'ST, MT', null, 'FTI', 'III/b', null, null, null, null, 'DPY', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('GHAR', 'G. Harjanto', 'Ir.', '', null, null, null, null, '9', '3', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('GSAN', 'Gatot Santoso', 'Ir.', 'MT.', '94 0865 494 E', null, 'IV/c', 'Lektor Kepala', '0', '0', '0', 'DPY', null, null, null, '0503086501', '04', '700');
INSERT INTO "public"."dosen" VALUES ('GUNS', 'Gunawan Budi Susilo', null, 'S.Pd.T., M.Eng.', '15 0486 731 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0523048602', '03', null);
INSERT INTO "public"."dosen" VALUES ('HAFS', 'Hafsah', null, 'S.Si.,MT.', null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HARM', 'Harmastuti', 'Dra.', 'M.Kom.', '87 0659 316 E', null, 'III/c', 'Lektor ', '27', '11', '0', 'DPY', '2', null, null, '0522095901', '07', '200');
INSERT INTO "public"."dosen" VALUES ('HART', 'Suharto', 'Drs. H.', 'MM.', null, null, 'IV/a', 'Lektor', '8', '4', '0', 'Luar', '5', null, null, '9905003422', '03', null);
INSERT INTO "public"."dosen" VALUES ('HAST', 'Hastutiningrum', '', 'ST.', null, null, 'III/d', 'Lektor', '0', '0', '0', 'DPY', null, 'keliru', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HBER', 'HAMZAH BERAHIM', 'Prof.Dr.Ir.H.', 'MT.', null, null, 'IV/d', 'Guru Besar', '3', '1', '0', null, null, null, null, '0022054502', null, null);
INSERT INTO "public"."dosen" VALUES ('HBUDS', 'Haryono Budi Santosa', 'Ir.', '', null, 'FTI', null, 'Lektor Kepala', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HER', 'Sigit Hernowo', 'Ir.', 'M.Kes', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HERM', 'Hermawan', 'Ir.', 'M.Si.', null, null, null, 'Lektor', '6', '3', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HFAB', 'Haider F. Abdul Amir', 'DR.', 'M.Sc.', null, null, null, 'Lektor', '6', '2', '0', 'DK', null, 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HHAM', 'H. Hamdani AK', '', 'SH.CN', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HISAN', 'Hichmat Santoso', '', 'SE.', null, null, 'III/b', 'Asisten Ahli', '31', '15', '4', 'DPY', '4', 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HJUL', 'H. Julianus', 'Ir.', 'MSIE', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HKAS', 'H. Kasmidi', 'Drs. Kol. (Purn)', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HMAH', 'Haidar Mahdi Hussain', '', 'MT.', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HPRA', 'Hadi Prasetyo Suseno', '', 'ST., M.Si.', '83 1058 207 E', null, 'III/d', 'Lektor', '10', '5', '0', 'DPY', '1', null, null, '0505105801', '11', '300');
INSERT INTO "public"."dosen" VALUES ('HRYT', 'Heriyanto', null, 'ST., M.Cs', null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HSAN', 'Heru Santoso', 'DR. Ir.', 'Br.M.Eng', null, null, null, null, '2', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HSAP', 'Hadi Saputra', '', 'S.T.,M.Eng', '11 0769 667 E', 'FTI', 'III/b', 'Asisten Ahli ', null, null, null, 'DPY', null, null, null, '0525076903', '03', '150');
INSERT INTO "public"."dosen" VALUES ('HSUB', 'Heru Subaris', '', 'SKM, M.Kes.', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HSUL', 'Harry Sulistyo', 'Ir.', 'SU.,Ph.D.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HSUP', 'Harri Supriyono', '', 'SH,M.Si.', null, null, null, 'Asisten Ahli', '16', '8', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HUTA', 'Herny Utami', '', 'M.Si', null, null, null, null, '8', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HWIB', 'Hary Wibowo', 'Ir.', 'MT.', '89 0661 379 E', null, 'III/d', 'Lektor', '27', '10', '0', 'DPY', '2', null, null, '0529066101', '03', '300');
INSERT INTO "public"."dosen" VALUES ('HWID', 'Hartono Widodo', 'Ir.', '', null, null, null, null, '2', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('HWIO', 'Harry Prabowo', null, 'ST, MT', null, 'FTI', 'III/b', 'Lektor', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('IBBU', 'IBRAHIM BUDI', null, null, null, null, null, null, null, null, null, null, null, null, null, '9905002746', '03', null);
INSERT INTO "public"."dosen" VALUES ('IBUY', 'Irawadi Buyung', 'Ir.', 'MT.', null, null, 'III/d', 'Lektor', '18', '8', '0', 'DPY', '2', 'Mengundurkan diri', '7', null, null, null);
INSERT INTO "public"."dosen" VALUES ('IDEW', 'Ismiradewi', null, 'S.Psi., M.Psi.', null, null, null, null, null, null, null, 'Luar', null, null, null, '0511058301', '03', null);
INSERT INTO "public"."dosen" VALUES ('IGUG', 'I Gusti Gde Badrawada', '', 'S.T., M.Eng', '03 0769 583 E', '', 'III/c', 'Lektor ', null, null, null, 'DPY', null, null, '8', '0526076901', '33', '200');
INSERT INTO "public"."dosen" VALUES ('IISR', 'Ismadi Isran', 'Ir.', 'MT.', null, null, null, null, '6', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('IKET', 'I Ketut Tada', '', 'S.Sos', null, null, 'III/b', 'Asisten Ahli', '8', '4', '6', 'Luar', null, null, null, '9905005203', null, null);
INSERT INTO "public"."dosen" VALUES ('IKUS', 'Indraswari Kusumaningtyas', '', 'ST.', null, 'FTI', null, 'Asisten Ahli', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('IMAD', 'I Made Bendiyasa', 'Ir.', 'MSc., PhD.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('IMAS', 'I Made Suardjaja', 'DR. Ir.', 'M.Sc.', null, null, 'IV/c', 'Lektor Kepala', '6', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('IMIA', 'I Made Miasa', '', 'ST.,M.Sc.', null, 'FTI', null, 'Asisten Ahli', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('INDA', 'Indarto', 'Dr. Ir.', 'DEA', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('INEN', 'I Nengah Sumerti', 'Ir.', '', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('INYO', 'I Nyoman Warta', 'Drs.', 'M.Hum', null, null, 'IV/a', 'Lektor', '8', '4', '6', 'Luar', null, null, null, '9905001739', null, null);
INSERT INTO "public"."dosen" VALUES ('ISNA', 'Isnaini Bs', 'Ir.', '', null, null, null, null, '2', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ISOD', 'Imam Sodikin', '', 'S.T,MT.', '95 1270 509 E', null, 'IV/b', 'Lektor Kepala ', '0', '0', '0', 'DPY', '2', null, '7', '0524127002', '02', '550');
INSERT INTO "public"."dosen" VALUES ('ISOE', 'Ign. Soedarno', 'Ir.', 'MT.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ISUDJ', 'Iman Sudjoko', 'Ir.', '', null, 'FTI', null, 'Lektor', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ISUR', 'Ignatius Suraya', 'Drs.', 'M.Cs.', '87 0955 312 E', null, 'III/c', 'Lektor ', '21', '7', '3', 'DPY', null, null, null, '0524095501', '07', '200');
INSERT INTO "public"."dosen" VALUES ('ISWA', 'Iswanto', '', 'S.Pd', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ITAH', 'Iqmal Tahir', 'Drs.', 'M.Si.', null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('IUTI', 'Ismun Uti Adan', 'Ir.', '', null, 'FTI', null, 'Lektor Kepala', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('IWAH', 'Iman Wahyono Sumarinda', 'Ir.', 'MSC.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('IWID', 'Inti Widi Prasetyanto', 'Ir.', '', '92 0364 449 E', null, 'III/d', 'Lektor ', '9', '4', '0', 'DPY', null, null, null, '0502036401', '10', '200');
INSERT INTO "public"."dosen" VALUES ('JATM', 'Soedjatmiko', 'Ir.', 'M.Sc.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('JIRI', 'Joko Irianto', 'Ir.', '', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('JMUL', 'Juariah Mulyanti', 'Ir.', 'M.Sc.', null, '', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('JON', 'H. Joni Arifin', 'Ir.', '', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('JPAR', 'Janu Pardadi', 'Ir.', '', null, null, null, null, '6', '3', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('JSUS', 'Joko Susetyo', 'Ir.', 'M.T.', '93 0961 474 E', null, 'IV/a', 'Lektor Kepala', '0', '0', '0', 'DPY', '2', null, '6', '0511096102', '32', '400');
INSERT INTO "public"."dosen" VALUES ('JTRI', 'Joko Triyono', '', 'S.Kom M.Cs', '89 0867 395 E', null, 'III/b', 'Asisten Ahli ', null, null, null, 'DPY', null, null, null, '0506086702', '35', '150');
INSERT INTO "public"."dosen" VALUES ('JWAL', 'Joko Waluyo', 'Ir.', 'MT.', '89 0361 380 E', null, 'IV/a', 'Lektor Kepala ', '26', '10', '0', 'DPY', '3', null, '5', '0516036101', '03', '400');
INSERT INTO "public"."dosen" VALUES ('JWIJ', 'Jarot Wijayanto', '', 'ST.,M.Eng.', null, 'FTI', 'III/b', 'Asisten Ahli', null, null, null, 'DPY', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('KAEL', 'Kaelan', 'Drs.', 'SU.', null, null, null, null, '8', '4', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('KARTI', 'Kartiko', 'Dr.Drs.', 'M.Si', null, null, null, 'Lektor Kepala', null, null, null, 'DPY', null, null, null, '8828830017', '06', null);
INSERT INTO "public"."dosen" VALUES ('KASM', 'Kol (Purn) Kasmidi', 'Drs.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('KHAN', 'Kokoh Handoko', null, 'S.T.', null, null, null, null, null, null, null, 'DPY', null, 'Keluar', null, '0508017301', '33', null);
INSERT INTO "public"."dosen" VALUES ('KHAR', 'Koesnadi Harjosumantri', 'Prof.', 'SH', null, null, null, 'Guru Besar', '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('KMUH', 'Khairul Muhajir', 'H. Drs.', 'M.T.', '19560909 198303 1 001', null, 'IV/a', 'Lektor Kepala', '10', '5', '0', 'DPK', '3', null, null, null, '03', '550');
INSERT INTO "public"."dosen" VALUES ('KRIS', 'Kris Tri Basuki', 'Dr. Ir.', 'M.Sc.APU', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('KSUR', 'Kris Suryowati', '', 'S.Si.,M.Si.', '19710626 199702 2 001', 'MIPA', 'III/d', 'Lektor', null, null, null, 'DPK', null, null, null, '0026067102', '06', '300');
INSERT INTO "public"."dosen" VALUES ('KURN', 'Kurniati', 'Dra.', 'M.Sc', null, null, 'III/c', 'Lektor', '8', '4', '0', 'DPY', '2', 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('KUSM', 'Kusmono', '', 'ST.,MT.', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('KWIJ', 'Karna Wijaya', 'DR.', '', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('LAB', 'Laboratorium', '', '', null, null, null, null, '17', '15', '0', 'TIM', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('LPM', 'LPM', '', '', null, null, null, null, '6', '3', '0', 'TIM', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MAEN', 'Muh. Arif Eko Nurcahyo,', '', 'S.T.', null, 'FTI', null, null, null, null, null, null, null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MAKH', 'Makhmudun A.', 'Ir.', 'M.Si', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MAND', 'Muhammad Andang Novianta', '', 'ST.,MT.', '03 1169 584 E', 'FTI', 'IV/a', 'Lektor Kepala ', null, null, null, 'DPY', null, null, null, '0520116901', '34', '400');
INSERT INTO "public"."dosen" VALUES ('MARG', 'Margono', 'Drs.', '', null, null, 'III/b', null, '0', '0', '0', 'DPY', '2', 'Pensiun', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MASR', 'Mochtar Asroni', 'Ir.', 'MSME', null, null, null, null, '0', '0', '0', 'DPY', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MDID', 'M.Didik Rochmat Wahyudi', '', 'ST.,MT.', null, 'FTI', 'III/b', 'Asisten Ahli', null, null, null, 'DPY', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MEGA', 'Mega Inayati Rif`ah', null, 'S.T., M.Sc', '16 0789 751 E', 'FTI', 'III/b', null, null, null, null, 'DPY', null, null, null, '0522078903', '02', null);
INSERT INTO "public"."dosen" VALUES ('MIFT', 'Miftahussalam', 'Ir.', 'M.T.', '87 0254 317 E', null, 'IV/a', 'Lektor Kepala ', '9', '4', '0', 'DPY', '2', null, '5', '0528025301', '10', '400');
INSERT INTO "public"."dosen" VALUES ('MKHI', 'Muhammad Khotibul Umam HS.', 'Ir.', 'MT.', null, null, null, 'Asisten Ahli', null, null, null, 'Luar', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MMUC', 'M. Muchalal', 'DR', '', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MSAJ', 'Moedjiana Sajidi', 'Ir.', 'MT.', null, null, 'III/d', 'Lektor', '12', '6', '0', 'DPY', '4', 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MSHO', 'Muhammad Sholeh', '', 'ST.MT.', '94 1269 498 E', null, 'IV/b', 'Lektor Kepala ', '10', '4', '0', 'DPY', '2', null, '6', '0510126902', '05', '550');
INSERT INTO "public"."dosen" VALUES ('MSUY', 'Muhammad Suyanto', 'Ir.', 'MT.', '89 0760 378 E', null, 'IV/a', 'Lektor Kepala', '30', '12', '0', 'DPY', '2', null, null, '0504076001', '34', '400');
INSERT INTO "public"."dosen" VALUES ('MTAR', 'Muhammad Tari', 'Drs.', 'M.Si', null, null, 'III/c', 'Lektor', '6', '2', '3', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MTHO', 'M.Thoifur', 'Drs.', 'M.Si', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MTIT', 'Maria Titah Jatipaningrum', '', 'S.Si., M.Sc.', '13 0583 687 E', null, 'III/b', 'Asisten Ahli', null, null, null, 'DPY', null, null, null, '0512058301', '06', null);
INSERT INTO "public"."dosen" VALUES ('MTOI', 'Mohammad Toifur', 'Dr', null, null, 'FTI', 'III/d', 'Lektor Kepala', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MUAZ', 'Muhammad Aziz', null, 'S.T., M.T', null, 'FTM', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MUCH', 'Muchlis', 'Dr.', 'SP. M.Sc', '12 0274 685 E', null, 'III/c', 'Lektor ', null, null, null, 'DPY', null, null, null, '0509027401', '10', '200');
INSERT INTO "public"."dosen" VALUES ('MUDJ', 'Mudjijana', 'Ir.', 'M.ENG', null, null, null, 'Lektor', '6', '3', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MUJI', 'Mujiman', '', 'ST.,MT.', '84 0754 232 E', null, 'III/c', 'Lektor', '23', '11', '0', 'DPY', '1', null, null, '0505075501', '34', '200');
INSERT INTO "public"."dosen" VALUES ('MUTI', 'Mutijo', null, 'S.Si, M.Si ', null, null, null, null, null, null, null, 'Luar', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('MYUN', 'Murni Yuniwati', 'Ir.', 'MT.', '88 0661 344 E', null, 'IV/a', 'Lektor Kepala', '20', '8', '0', 'DPY', '2', null, '7', '0511066101', '01', '400');
INSERT INTO "public"."dosen" VALUES ('MYUS', 'Muhammad Yusuf', 'Ir.', 'MT.', '84 1064 218 E', null, 'III/c', 'Lektor', '9', '4', '0', 'DPY', null, null, null, '0514106401', '32', '200');
INSERT INTO "public"."dosen" VALUES ('NARS', 'Narsito', 'DR.', '', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('NART', 'Narto', '', 'BE,', null, 'MIPA', null, 'Lektor Kepala', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('NEILA', 'Neila Ramadhani', 'Dra.', 'M.Si, M.Sc', null, null, null, 'Lektor', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('NELB', 'Nelly B.', 'Ir. Hj.', 'MSIE', null, null, null, 'Lektor Kepala', '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('NHAY', 'Nur Hayati', null, 'S.T., M.Eng.', null, null, null, null, null, null, null, 'DPY', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('NHER', 'Nuniek Herawati', 'Dra.', 'M.Kom.', '91 1261 418 E', null, 'III/c', 'Lektor ', '9', '3', '0', 'DPY', '2', null, null, '0531126101', '07', '200');
INSERT INTO "public"."dosen" VALUES ('NHID', 'Nurul Hidayat Aprilia', '', 'S.Si,M.Si', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('NKAB', 'Naksir Kaban', 'Ir.', '', null, null, null, 'Lektor Kepala', '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('NLES', 'Nidia Lestari', null, 'S.T, M.Eng', '14 1187 705 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0526118703', '03', null);
INSERT INTO "public"."dosen" VALUES ('NMBA', 'Noor M. Bakri', 'Drs.', '', null, null, null, 'Lektor Kepala', '6', '3', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('NOER', 'Noeryanti', 'Dra.', 'M.Si.', '19580426 198703 2 002', null, 'IV/c', 'Lektor Kepala ', '27', '9', '0', 'DPK', '1', null, null, '0026045802', '06', '700');
INSERT INTO "public"."dosen" VALUES ('NORO', 'Noerochman', 'Drs.', 'M.Kom.', null, null, null, null, null, null, null, 'Luar', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('NOVI', 'Noviana Pratiwi', null, 'S.Si.,M.Sc.', '14 0386 703 E', null, 'III/b', 'Asisten Ahli', null, null, null, 'DPY', null, null, null, '0505038601', '06', '150');
INSERT INTO "public"."dosen" VALUES ('NSAK', 'Nursakti Ningrum', 'Dra.', 'M.Hum', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('NURY', 'Nuryono', 'DR.', '', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('NWID', 'Naniek Widyastuti', 'Dra.', 'MT.', '19551005 198803 2 001', null, 'IV/a', 'Lektor Kepala', '19', '7', '0', 'DPK', '4', null, null, '0005105502', '05', '550');
INSERT INTO "public"."dosen" VALUES ('PARD', 'Soepardiman', 'Ir. H.', '', null, null, null, null, '0', '0', '0', 'DPY', null, 'Pensiun', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PAWI', 'PANCA ARI WIBOWO', null, null, null, null, null, null, null, null, null, null, null, null, null, '9905004801', '03', null);
INSERT INTO "public"."dosen" VALUES ('PBAS', 'Panggih Basuki', 'Drs.', 'M.IKom', null, null, null, null, '6', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PBIS', 'Prabuditya Bisma Wisnu', null, 'S.T., M.Eng.', '15 0386 736 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0529038601', '03', null);
INSERT INTO "public"."dosen" VALUES ('PDSI', 'Paramita Dwi Sukmawati', '', 'S.T., M.Eng.', '15 0987 720 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0528098701', '11', null);
INSERT INTO "public"."dosen" VALUES ('PEKO', 'Prastyono Eko Pambudi', 'Ir.', 'M.T.', '89 0461 394 E', null, 'IV/a', 'Lektor Kepala', '31', '12', '0', 'DPY', '3', null, '3', '0519046101', '04', '400');
INSERT INTO "public"."dosen" VALUES ('PHAR', 'Prita Haryani', null, 'S.Pd., M.Eng.', '15 0190 732 E', null, 'III/b', 'Asisten Ahli', null, null, null, 'DPY', null, null, null, '0525019001', '05', null);
INSERT INTO "public"."dosen" VALUES ('PRAC', 'Purwanto Rachmat', 'Pdt.', 'STh.', null, null, 'IV/a', 'Lektor Kepala', '12', '6', '8', 'Luar', null, null, null, '9905000655', null, null);
INSERT INTO "public"."dosen" VALUES ('PRAJ', 'Prajitno', 'Ir.', 'MT.', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PRAM', 'Pramiyati', 'Dra.', '', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PRAP', 'Suprapto', 'Drs.', 'M.Kom', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PRIY', 'Priyana', 'Ir.', 'M.Sc.', null, null, 'IV/d', null, '12', '4', '3', 'DPY', '8', 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PRIYA', 'Priyatmadi', 'Ir.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PRWN', 'Purnawan', '', 'ST.,M.Eng.', '83 1062 190 E', null, 'III/c', 'Lektor', null, null, null, 'DPY', null, null, null, '0508106202', '11', '200');
INSERT INTO "public"."dosen" VALUES ('PRWO', 'Purwito', 'Drs.', '', null, null, 'III/c', 'Lektor', null, null, null, 'Luar', null, null, null, '0013045801', null, null);
INSERT INTO "public"."dosen" VALUES ('PSUM', 'Pc. Sumardi', 'Ir.', 'SU', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PTAM', 'P. Tamtomo', 'Ir.', 'M.Eng.', null, null, null, 'Lektor Kepala', '12', '5', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PUDJ', 'Pudjijanto', 'Ir.', 'MT.', null, null, 'III/b', 'Asisten Ahli', '3', '1', '0', 'DPY', '2', 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PURB', 'Purbawati', '', 'S.T.,M.T.', null, null, 'III/c', 'Asisten Ahli', '0', '0', '0', 'DPY', '2', 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PURN', 'Purnomo', 'Ir.', 'MSME, Ph.D.', null, null, 'IV/c', 'Lektor Kepala', '0', '0', '0', 'DPY', null, null, null, '8892930017', '03', null);
INSERT INTO "public"."dosen" VALUES ('PURW', 'Purwadi', '', 'ST.', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PWID', 'Petrus Widikarsana', 'Drs. ', '', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('PWIS', 'Petrus Wisnubroto', 'Drs.', 'M.Si.', '19550511 198703 1 001', null, 'IV/a', 'Lektor Kepala', '10', '4', '0', 'DPK', null, null, null, '0011055501', '32', '550');
INSERT INTO "public"."dosen" VALUES ('RADE', 'Risma Adelina Simanjuntak', 'Ir.', 'MT.', '88 0161 355 E', null, 'IV/a', 'Lektor Kepala', '24', '11', '0', 'DPY', null, null, null, '0502016102', '02', '400');
INSERT INTO "public"."dosen" VALUES ('RADIT', 'Radhitya Adzan Hidayah', null, 'S.T., M.Eng.', '15 0582 734 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0524058201', '10', null);
INSERT INTO "public"."dosen" VALUES ('RAYA', 'Renna Yanwastika Ariyana', null, 'S.T., M.Kom.', null, null, null, null, null, null, null, 'DPY', null, null, null, null, '05', null);
INSERT INTO "public"."dosen" VALUES ('RDBEK', 'Rokhana Dwi Bekti', null, 'S.Si., M.Si.', '15 0386 710 E', null, 'III/b', 'Asisten Ahli ', null, null, null, 'DPY', null, null, null, '0306038601', '06', '150');
INSERT INTO "public"."dosen" VALUES ('RDHA', 'Rini Dharmastiti', 'Ir.', 'M.Sc., Ph.D.', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('RDWI', 'Ronny Dwi Agusulistyo', 'Ir.', 'MT.', null, null, null, null, '0', '0', '0', 'DPY', null, 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('RESP', 'Respati', 'Ir.', '', null, null, null, null, '6', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('RHAR', 'Rochmad Haryanto', '', 'S.Kom.', null, null, 'III/a', '', null, null, null, 'DPY', null, 'Keluar', null, '0519118302', '06', null);
INSERT INTO "public"."dosen" VALUES ('RHART', 'Rudy Hartanto', 'Ir.', 'MT.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('RIFN', 'Rizqi Fitri Naryanto', null, 'ST., M.Eng', null, null, null, null, null, null, null, 'DPY', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('RISM', 'Rida Ismu', 'Ir.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('RKHAS', 'Rahayu Khasanah', null, 'S.T., M.Eng.', '14 0190 701 E', null, 'III/b', 'Asisten Ahli', null, null, null, 'DPY', null, null, null, '0526019001', '02', '150');
INSERT INTO "public"."dosen" VALUES ('RKUM', 'Rosalia Arum Kumalasanti', null, 'S.T., M.T.', '15 0589 733 E', null, 'III/b', 'Asisten Ahli', null, null, null, 'DPY', null, null, null, '0509058901', '05', null);
INSERT INTO "public"."dosen" VALUES ('ROCH', 'Rochmadi', 'Ir.', 'SU. PhD.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('RSOE', 'R. Soekrisno', 'Dr.,Ir.', 'MSME', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('RSRI', 'Rachmat Sriwijaya', '', 'ST., MT.', null, null, null, null, '9', '3', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('RUBA', 'Rubahman', 'Ir.', '', null, null, 'IV/b', null, '0', '0', '0', 'DPY', '6', 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('RWAR', 'Retantyo Wardoyo', 'DR.', '', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('RWRO', 'R WARNO', null, null, null, null, null, null, null, null, null, null, null, null, null, '9905003432', '03', null);
INSERT INTO "public"."dosen" VALUES ('RYUL', 'Rr Y Rachmawati Kusumaningsih', '', 'ST., MT.', '96 0770 519 E', null, 'III/d', 'Lektor ', '8', '3', '0', 'DPY', null, null, null, '0512077001', '35', '300');
INSERT INTO "public"."dosen" VALUES ('SACH', 'Sayid Achmad', 'Ir.', '', null, null, null, 'Lektor Kepala', '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SAHQ', 'SAIFUDDIN HAQ', null, null, null, null, null, null, null, null, null, null, null, null, null, '9905004160', '03', null);
INSERT INTO "public"."dosen" VALUES ('SAMB', 'Suprih Ambawani', 'Dra.', 'M.Pd', '92 1266 450 E', null, 'III/b', 'Asisten Ahli', '12', '6', '0', 'DPY', null, null, null, '0521126601', '33', '100');
INSERT INTO "public"."dosen" VALUES ('SAMS', 'Samsudin', 'Ir.', '', null, null, null, 'Lektor Kepala', '4', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SAPRI', 'Samin Prihatin', 'Prof. Drs.', 'APU', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SARIF', 'Subhan Arif', null, 'S.T', '16 0491 753 E', 'FTM', 'III/a', null, null, null, null, 'DPY', null, null, null, null, '10', null);
INSERT INTO "public"."dosen" VALUES ('SARJ', 'Sarjuni', '', 'SH.', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SART', 'Sarto', 'Ir.', 'M.Sc.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SASP', 'Sasongko P.', 'DR. Ir.', 'DEA', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SDARM', 'Suryo Darmo', 'Ir.', 'MT.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SDIB', 'Soedibyo', 'Ir.', 'M.Sc.', null, null, 'IV/b', 'Asisten Ahli', '2', '1', '0', 'DPY', '6', 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SDIBY', 'Soedibyo', 'KOL. (Purn.)', '', null, null, 'IV/b', null, '0', '0', '0', 'Luar', '6', 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SEKAR', 'Kartinasari Ayuhikmatin Sekarjati', null, 'S.T., M.Sc.', null, null, 'III/b', null, null, null, null, 'DPY', null, null, null, null, '02', null);
INSERT INTO "public"."dosen" VALUES ('SEPVI', 'Septian Vienastra', null, 'S.Si, M.Eng', '15 0985 737 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0514098502', '10', null);
INSERT INTO "public"."dosen" VALUES ('SETI', 'Setiaji', 'Prof. Drs.', 'SU', null, null, null, 'Guru Besar', '6', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SFAT', 'Sri Fatimah', 'Ir.', '', null, null, null, null, '6', '3', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SGYN', 'Sugiyono', '', 'ST.,MT.', null, 'FTI', null, 'Asisten Ahli', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SHAN', 'Slamet Hani', '', 'ST.,MT.', '96 1260 522 E', null, 'IV/a', 'Lektor Kepala', '12', '5', '0', 'DPY', '2', null, null, '0530126002', '04', '400');
INSERT INTO "public"."dosen" VALUES ('SHAR', 'Sri Haryatmi', 'DR.', 'M.Stat.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SHAS', 'Sri Hastutiningrum', '', 'ST.,M.Si.', '95 0558 500 E', null, 'III/d', 'Lektor', '13', '8', '0', 'DPY', '2', null, null, '0524055801', '11', '200');
INSERT INTO "public"."dosen" VALUES ('SHUD', 'Saiful Huda', 'Ir.', 'MT.,ME.', '88 0256 361 E', null, 'IV/a', 'Lektor Kepala', '16', '6', '0', 'DPY', '3', null, '2', '0528025602', '03', '400');
INSERT INTO "public"."dosen" VALUES ('SHUS', 'Salahudin Husein', 'Dr.', 'ST, MT', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SIAN', 'Stuart Ian Wattam', '', 'M.Sc.', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SIDH', 'Sidarto', 'Drs.', 'M.Eng.', null, null, 'III/c', 'Lektor', '16', '7', '0', 'DPY', null, 'Meninggal', null, '0525125501', null, null);
INSERT INTO "public"."dosen" VALUES ('SIHAN', 'Sirod Hantoro', 'Drs.', 'MSIE', null, null, 'IV/d', 'Lektor Kepala', '6', '2', '0', 'DPY', null, 'Keluar', null, '9900000958', null, null);
INSERT INTO "public"."dosen" VALUES ('SIMS', 'Siti Imsyawati Maulidya', 'Hj.', 'S.T.,M.Kom.', null, null, 'III/c', 'Lektor', '26', '11', '0', 'DPY', null, null, null, '0504057102', '05', null);
INSERT INTO "public"."dosen" VALUES ('SJUA', 'Sri Juari', 'DR.', 'M.Eng.', null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SKAM', 'Samsul Kamal', 'Dr.Ir.', 'M.Sc.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SKRI', 'Samuel Kristiyana', 'Dr.', 'ST., MT.', '98 1270 530 E', null, 'III/b', 'Asisten Ahli', '19', '9', '0', 'DPY', '1', null, null, '0506127002', '34', '100');
INSERT INTO "public"."dosen" VALUES ('SLUW', 'Sri Luwihana D.', 'Ir.', 'SU', null, null, null, null, '0', '0', '0', 'DPK', null, 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SMUL', 'Sri Mulyaningsih', 'Dr.', 'S.T., M.T.', '96 0672 516 E', null, 'IV/a', 'Lektor Kepala', '0', '0', '0', 'DPY', '2', null, null, '0503067201', '10', '400');
INSERT INTO "public"."dosen" VALUES ('SMUR', 'Soekardi Muryowihardjo', 'Ir.', '', null, null, 'III/c', 'Lektor', '2', '1', '0', 'Luar', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SOEB', 'Soebanar', 'Prof.', 'Ph.D.', null, null, null, 'Guru Besar', '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SOEBR', 'Soebronto', 'Ir.', '', null, null, null, null, '0', '0', '0', 'DPY', null, 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SOED', 'Soedarno', 'Ir.', 'MT.', null, null, null, null, '2', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SOEH', 'Soeharto', 'DR.', null, null, null, null, 'Lektor', '6', '2', null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SOET', 'Soetoto', 'Ir.', 'SU.', null, null, null, 'Lektor Kepala', '3', '1', '0', 'Luar', null, null, null, '9900006477', null, null);
INSERT INTO "public"."dosen" VALUES ('SOLE', 'Solechan', null, 'ST, MT', null, 'FTI', null, 'Asisten Ahli', null, null, null, 'Luar', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SPAN', 'Sri Pangesti', 'Dra.', 'SU', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SPOE', 'Soetomo Poerodiprodjo', 'Ir.', '', null, null, null, null, '6', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SPRA', 'Subagyo Pramumijoyo', 'DR. Ir.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SPRI', 'Sigit Priyambodo', '', 'ST.,MT.', '96 0967 521 E', null, 'III/d', 'Lektor', '8', '3', '0', 'DPY', '2', null, null, '0518096703', '04', '300');
INSERT INTO "public"."dosen" VALUES ('SRAH', 'Suwanto Raharjo', '', 'S.Si, M.Kom.', '03 0775 579 E', 'FTI', 'III/d', 'Lektor ', null, null, null, 'DPY', null, null, null, '0519077501', '05', '300');
INSERT INTO "public"."dosen" VALUES ('SRGI', 'Sri Rahayu Gusmarwani', '', 'S.T., M.T.', '13 0771 692 E', null, 'III/c', 'Lektor ', null, null, null, 'DPY', null, null, null, '0511077101', '01', '200');
INSERT INTO "public"."dosen" VALUES ('SRIW', 'Sritomo W.', 'Ir.', 'M.Sc.', null, null, null, null, '2', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SRIY', 'Sriyono', 'Ir.', 'MS.', null, null, 'IV/b', 'Lektor Kepala', '4', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SROH', 'Siti Rohana Nasution', 'Ir.', 'MT.', null, null, 'III/a', 'Asisten Ahli', '22', '8', '0', 'DPK', '2', 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SSAIN', 'Salahuddin Husain', 'Dr.', 'S.T., M.Sc.', null, null, 'IV/a', 'Lektor Kepala', null, null, null, 'Luar', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SSAN', 'Siwi Sanjoto', 'Ir. H.', 'M.T.', null, null, 'III/c', 'Lektor', '3', '1', '0', 'DPY', '2', 'Meninggal', null, '0531055501', '10', null);
INSERT INTO "public"."dosen" VALUES ('SSAU', 'Siti Saudah', '', 'S.Pd.,M.Hum.', '96 0271 512 E', null, 'III/d', 'Lektor ', '4', '2', '0', 'DPY', '2', null, null, '0515027101', '32', '300');
INSERT INTO "public"."dosen" VALUES ('SSET', 'Sigit Setyawan', '', 'ST', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SSIR', 'Sepridawati Siregar', null, 'S.Si, M.T', '14 0978 704 E', null, 'III/b', null, null, null, null, 'DPY', null, null, null, '0529097803', '10', null);
INSERT INTO "public"."dosen" VALUES ('SSOE', 'Soekardiman Soelindrijo', 'Ir.', '', null, null, 'IV/b', 'Lektor', '9', '3', '0', 'DPY', '7', 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SSRI', 'Suprihastuti Sri Rahayu', 'Ir.', 'M.Sc.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SSUD', 'Sri Sudiono', '', 'M.Si.', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SSUM', 'Sri Sumarsih', 'Dra.', 'M.Si.', null, null, 'III/b', 'Asisten Ahli', '0', '0', '0', 'DPY', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SSUMO', 'SUGENG SUMARDJONO', null, null, null, null, null, null, null, null, null, null, null, null, null, '0509066004', '04', null);
INSERT INTO "public"."dosen" VALUES ('SSUN', 'Sri Sunarsih', 'Dra.', 'M.Si.', '91 0462 434 E', null, 'III/b', 'Asisten Ahli ', '8', '3', '0', 'DPY', '2', null, null, '0510046201', '11', '100');
INSERT INTO "public"."dosen" VALUES ('SSUR', 'Sri Suryani', 'Ir.', 'SU', null, null, null, 'Lektor Kepala', '4', '2', '0', 'Luar', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SSUY', 'Slamet Suyono', 'Ir.', 'M.T.', null, null, 'III/b', 'Lektor', '2', '1', '0', 'DPY', '2', 'Meninggal', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUBA', 'Subandi', '', 'ST.,MT.', '86 1158 292 E', null, 'III/c', 'Lektor', '20', '8', '0', 'DPY', '2', null, null, '0527105801', '34', '200');
INSERT INTO "public"."dosen" VALUES ('SUDI', 'Sudibyo', '', 'M.Si', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUGI', 'Soegiharto', 'Ir.', '', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUGM', 'Fx. Sugianto', 'Ir.', 'M.Eng,', null, 'MIPA', null, 'Asisten Ahli', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUGP', 'Sugijarto Prawirosentono', 'Ir.', '', null, null, 'IV/d', 'Lektor Kepala', '6', '2', '0', 'DPY', null, null, null, '9900979996', '03', null);
INSERT INTO "public"."dosen" VALUES ('SUHAD', 'Suhadijono', 'Ir.', '', null, null, null, 'Lektor Kepala', '13', '5', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUHAN', 'Suhanan', 'DR.Ir.', 'DEA', null, null, null, null, '9', '3', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUHAR', 'Suhartono', 'Ir.', 'MT.', null, null, 'III/c', 'Lektor', '26', '12', '0', 'DPK', '3', 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUHP', 'Suheram P.', 'KOL. PURN.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUJI', 'Sujito', '', 'ST.,MT.', null, 'FTI', null, null, null, null, null, 'DPY', null, 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUKA', 'Sukamta', '', 'Ph.D', null, null, 'III/c', 'Lektor', '0', '0', '0', 'DPY', '1', 'Keluar', null, '0506046702', null, null);
INSERT INTO "public"."dosen" VALUES ('SUKAN', 'Sukandarrumidi', 'Prof. Ir.', 'M.Sc., Ph.D.', null, null, 'IV/e', 'Guru Besar', '2', '1', '0', 'DPY', null, null, null, '9900979997', '10', null);
INSERT INTO "public"."dosen" VALUES ('SUKAR', 'Sukardjo', 'Prof. Dr.', '', null, null, null, 'Guru Besar', '4', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUMA', 'Sumarni', 'Ir.', 'MS', '19530404 198203 2 003', null, 'IV/b', 'Lektor Kepala', '23', '7', '0', 'DPK', '4', null, '6', '0004045302', '01', '700');
INSERT INTO "public"."dosen" VALUES ('SUNA', 'Sunardjo', 'Ir.', 'MT.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUNJ', 'Sunjoto', 'Dr. Ir.', 'DEA', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUPH', 'Suprihastuti', 'Ir.', 'SU.', null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUPR', 'Supranto', 'Ir.', 'MSc. PhD.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SURO', 'Suraya', null, 'S.Si., M.Kom.', '91 0262 415 E', null, 'III/c', 'Lektor ', null, null, null, 'DPY', null, null, null, '0525026203', '05', '200');
INSERT INTO "public"."dosen" VALUES ('SUSUM', 'Sujoko Sumaryono', 'Ir.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUTR', 'Sutrisno', 'DR.Ir.', 'M.Sc', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUTRI', 'Sutrisno', '', 'ST,MT', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUW', 'SRI SUWARNO', 'Ir.', 'M.ENG', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUYA', 'Suyadi M.', 'Drs.', '', null, null, null, null, '4', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SUYAN', 'Suyanto', 'Drs.', 'M.Si.', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SWAH', 'Sri Wahyu Kusumastuti', 'Ir.', 'M.Si.', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SWED', 'Sagoro Wedy', 'Ir. H.', 'MM.', null, null, 'III/b', null, '0', '0', '0', 'DPY', '2', 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SWIB', 'Samekto Wibowo', 'Prof.Dr.', '', null, null, null, 'Guru Besar', '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SWIY', 'Sugeng Wiyono', 'Ir.', 'MS.', null, null, null, 'Lektor Kepala', '2', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SYAFA', 'Syafaruddin A.', 'Drs.', 'MS', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SYAFI', 'Syafriyudin', '', 'ST.,MT.', '95 0568 506 E', null, 'IV/a', 'Lektor Kepala', '8', '4', '0', 'DPY', null, null, null, '0518056803', '34', '400');
INSERT INTO "public"."dosen" VALUES ('SYAM', 'Siti Syamsiah', 'Ir', 'M.Sc', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('SYUK', 'Syukri Abdullah', 'Drs.', 'M.Hum', '90 0658 410 E', null, 'III/b', 'Asisten Ahli ', '6', '3', '0', 'DPY', '2', null, null, '0506065801', '03', '100');
INSERT INTO "public"."dosen" VALUES ('TADI', 'Tjahjana Adi', 'Ir.', 'MSME', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TAGU', 'Tri Agung Rohmat', 'Dr. Eng.', 'B.Eng,M.Eng', null, null, null, 'Asisten Ahli', '12', '4', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TARM', 'Tarmono', 'Ir.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TEAM', 'Team Dosen', '', '', null, null, null, null, '13', '4', '0', 'TIM', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('THAR', 'T. Haryono', 'Ir.', 'M.Sc', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('THAS', 'Talib Hashim Hasan', 'Dr.', 'M.Sc.', null, null, null, 'Lektor', null, null, null, 'DK', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('THID', 'Taufiq Hidayat', '', 'S.T., M.Eng', '04 0974 578 E', null, 'III/a', null, null, null, null, 'DPY', null, null, null, '0501097401', '03', null);
INSERT INTO "public"."dosen" VALUES ('TIM', 'TIM', '', '', null, null, null, null, '6', '5', '0', 'TIM', null, null, null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TISN', 'Titin Isna Oesman', 'Dr.Ir.Hj.', 'MM', '93 0650 475 E', null, 'IV/a', 'Lektor Kepala', '26', '11', '0', 'DPY', '3', null, null, '0506065001', '02', '400');
INSERT INTO "public"."dosen" VALUES ('TIYO', 'Tiyono', 'Ir.', '', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TKUN', 'Tri Kuntoro Pr', 'Drs.', 'M.Sc. EE', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TMAR', 'H. Tirun Marwito', '', 'SH.', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TNUR', 'Tiche Nurawati', '', 'SH,MH', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TOIF', 'Toifur', 'Drs.', 'M.Si', null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TOSU', 'Totok Sugiarto', 'Ir.', 'MSME', null, null, null, 'Lektor Kepala', '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TPIN', 'Tito Pinandita', 'S.Si.', null, null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TQC', 'Team Tqc', '', '', null, null, null, null, '0', '0', '0', 'TIM', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TRSG', 'Tri Sugiarto', null, 'SS', null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TRUS', 'Toto Rusianto', 'Dr. Ir.', 'M.T.', '93 0166 466 E', null, 'IV/b', 'Lektor Kepala', '23', '11', '0', 'DPY', null, null, null, '0512016601', '03', '550');
INSERT INTO "public"."dosen" VALUES ('TSAF', 'Triantoro Safaria', null, 'M.Si.', null, null, null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TSAN', 'Teguh Santoso', 'Ir.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TSRI', 'Thomas Sriwidodo', 'DR.', '', null, null, null, 'Guru Besar', '15', '5', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TSUG', 'Toto Sugiarto', 'Ir.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TUMI', 'Tumiran', 'DR. Ir.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('TWAH', 'Tri Wahyuningsih', '', 'ST', null, 'FTI', 'III/a', null, null, null, null, 'DPY', null, 'Mengundurkan diri', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('UBAS', 'Untung Basuki', null, 'S.Pd.T., M.Pd.', null, null, null, null, null, null, null, 'DPY', null, 'Keluar', null, null, '03', null);
INSERT INTO "public"."dosen" VALUES ('UJOK', 'Untung Joko Basuki', 'Drs.', 'M.Pd.I', '92 1263 455 E', null, 'III/b', 'Asisten Ahli', '10', '5', '0', 'DPY', '2', null, null, '0504126301', '33', '100');
INSERT INTO "public"."dosen" VALUES ('ULES', 'Uning Lestari', '', 'ST., M.Kom', '96 0870 520 E', null, 'IV/a', 'Lektor Kepala', '20', '8', '0', 'DPY', '2', null, null, '0531087001', '05', '400');
INSERT INTO "public"."dosen" VALUES ('UMIN', 'Uminingsih', 'Dra.', 'M.Kom.', '92 0660 448 E', null, 'III/c', 'Lektor ', '15', '5', '0', 'DPY', '2', null, null, '0506066003', '07', '200');
INSERT INTO "public"."dosen" VALUES ('UYAH', 'Utoro Yahya', 'DR.', 'M.Sc.', null, null, null, null, '6', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('VMAL', 'Victor Malau', 'Dr. Ir.', 'DEA', null, null, null, null, '10', '5', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('WAHP', 'Wahyu P.', 'DR. Ir.', 'MSIE', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('WARD', 'Wardoyo', 'Ir.', 'MT.', null, 'FTI', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('WBUD', 'Wahyudi Budi Sediawan', 'Ir.', 'SU. PhD', null, null, null, null, '2', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('WERDA', 'Bambang Suwerda', '', 'S.Si.', null, 'MIPA', null, null, null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('WHAN', 'Wiwik Handajadi', 'Ir.', 'M.Eng', '19520922 198003 1 001', null, 'IV/b', 'Lektor Kepala ', '26', '10', '0', 'DPK', '4', null, null, '0022095201', '04', '550');
INSERT INTO "public"."dosen" VALUES ('WINA', 'Winarto', 'DR.', 'M.Si', null, null, null, null, '3', '1', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('WINI', 'Winarni', 'Dra.', 'MM', '19551105 198603 2 001', null, 'III/a', 'Asisten Ahli ', '11', '5', '0', 'DPK', '1', null, '5', '0005115502', '32', '100');
INSERT INTO "public"."dosen" VALUES ('WIY', 'Wiyoto', 'Drs.', '', null, null, 'III/b', null, '17', '6', '0', 'DPY', '2', 'Meninggal', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('WNEG', 'Wisma Negara Christanto R', 'Drs.', 'M.Hum', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('WPUD', 'Widodo Pudji Muljanto', 'Ir.', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('WSUP', 'Wahyu Supartono', 'Dr.Ir', '', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('WWID', 'Wisnu Widiarto', '', 'S.Si,MT.', null, null, 'III/d', 'Lektor', '9', '3', '0', 'DPY', '2', 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('YKRIS', 'Yoseph Kristianto', '', 'SFK, M.Pd', null, 'FTI', 'III/c', 'Lektor', null, null, null, 'Luar', null, null, null, '0027116102', null, null);
INSERT INTO "public"."dosen" VALUES ('YMAR', 'Yoni Maristha', 'Drs.', 'MS', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('YPRA', 'Yuli Pratiwi', 'Dra.', 'M.Si.', '19640727 199003 2 002', null, 'IV/a', 'Lektor Kepala ', '15', '6', '3', 'DPK', '2', null, null, '0027076401', '11', '400');
INSERT INTO "public"."dosen" VALUES ('YPUR', 'Yuli Purwanto', '', 'S.T, M.Eng', null, null, 'III/a', '', null, null, null, 'DPY', null, '', null, null, '03', null);
INSERT INTO "public"."dosen" VALUES ('YRAM', 'Yuliastuti Ramadhani', 'Dra.', '', null, null, 'III/c', 'Lektor', '3', '1', '0', 'DPK', '3', null, null, '0013074803', '11', null);
INSERT INTO "public"."dosen" VALUES ('YSET', 'Yudi Setyawan', 'Drs.', 'M.S., M.Sc.', '02 1262 569 E', null, 'IV/a', 'Lektor Kepala ', null, null, null, 'DPY', '2', null, '5', '0517126202', '06', '400');
INSERT INTO "public"."dosen" VALUES ('YSUS', 'Y. Susanto', 'Ir.', 'MT.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('YULI', 'Hieronymus Yuliprianto', 'Prof. Dr. Ir', 'MS.', null, 'MIPA', 'IV/a', 'Guru Besar', null, null, null, 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ZSOE', 'Zanzawi Soeyoeti', 'Prof.DR.', '', null, null, null, 'Guru Besar', '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ZULA', 'Zulaela', 'Drs.', 'Dipl.Med., M.Si', null, null, null, null, '6', '2', '0', 'Luar', null, 'Keluar', null, null, null, null);
INSERT INTO "public"."dosen" VALUES ('ZULAI', 'Zulaila', 'Dra.', 'Dipl.Med.', null, null, null, null, '0', '0', '0', 'Luar', null, 'Keluar', null, null, null, null);

-- ----------------------------
-- Primary Key structure for table dosen
-- ----------------------------
ALTER TABLE "public"."dosen" ADD PRIMARY KEY ("kode");
