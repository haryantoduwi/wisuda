/*
Navicat PGSQL Data Transfer

Source Server         : 10.15.74.2-AKADEMIK
Source Server Version : 70407
Source Host           : 10.15.74.2:5432
Source Database       : akademik
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 70407
File Encoding         : 65001

Date: 2018-08-30 13:29:26
*/


-- ----------------------------
-- Table structure for prodi
-- ----------------------------
DROP TABLE "public"."prodi";
CREATE TABLE "public"."prodi" (
"kode" varchar(6) NOT NULL,
"fakultas" varchar(50) NOT NULL,
"jurusan" varchar(50) NOT NULL,
"pstudi" varchar(50) NOT NULL,
"status" varchar(15) NOT NULL,
"jenjang" varchar(10) NOT NULL,
"kodefak" varchar(4) NOT NULL,
"kajur" varchar(50),
"slh" varchar[],
"peringkat" varchar(2),
"sk" varchar(50),
"tglsk" date,
"dekan" varchar(6),
"kjur" varchar(6),
"kprodi" varchar(6),
"asalsk" text,
"fac" text,
"nip" varchar(25),
"dep" text,
"prodi" text,
"aktif" bool,
"email" varchar(50),
"www" varchar(100),
"gelar" varchar(8)
)
WITH OIDS 

;

-- ----------------------------
-- Records of prodi
-- ----------------------------
INSERT INTO "public"."prodi" VALUES ('00', 'Teknologi Industri', 'Teknik Kimia', 'Teknik Kimia', 'Akreditasi', 'S-1', 'FTI', 'MYUN', null, 'B', '013/BAN-PT/Ak-V/S1/VIII/2002', '2002-08-05', 'TRUS', 'MYUN', 'MYUN', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Industrial Technology', '89.0361.380.E', 'Chemical Engineering', 'Chemical Engineering', 'f', null, null, null);
INSERT INTO "public"."prodi" VALUES ('01', 'Teknologi Industri', 'Teknik Kimia', 'Teknik Kimia', 'Akreditasi', 'S-1', 'FTI', 'Sri Rahayu Gusmarwani, S.T., M.T.', null, 'B', '042/BAN-PT/Ak-XV/S1/XI/2012', '2012-11-23', 'TRUS', 'SRGI', 'SRGI', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Industrial Technology', '89.0361.380.E', 'Chemical Engineering', 'Chemical Engineering', 't', 'kimia@akprind.ac.id', 'kimia.akprind.ac.id', 'S.T.');
INSERT INTO "public"."prodi" VALUES ('02', 'Teknologi Industri', 'Teknik Industri', 'Teknik Industri', 'Akreditasi', 'S-1', 'FTI', 'Endang Widuri Asih, S.T., M.T.', null, 'B', '227/SK/BAN-PT/Ak-XVI/S/XI/2013', '2013-11-09', 'TRUS', 'EWID', 'EWID', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Industrial Technology', '89.0361.380.E', 'Industrial Engineering', 'Industrial Engineering', 't', 'teknikindustri@akprind.ac.id', 'industri.akprind.ac.id', 'S.T.');
INSERT INTO "public"."prodi" VALUES ('03', 'Teknologi Industri', 'Teknik Mesin', 'Teknik Mesin', 'Akreditasi', 'S-1', 'FTI', 'Nidia Lestari, S.T., M.Eng.', null, 'B', '017/BAN-PT/Ak-XV/S1/VI/2012', '2012-06-29', 'TRUS', 'NLES', 'NLES', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Industrial Technology', '89.0361.380.E', 'Mechanical Engineering', 'Mechanical Engineering', 't', 'mesin@akprind.ac.id', 'mesin.akprind.ac.id', 'S.T.');
INSERT INTO "public"."prodi" VALUES ('04', 'Teknologi Industri', 'Teknik Elektro', 'Teknik Elektro', 'Akreditasi', 'S-1', 'FTI', 'Sigit Priyambodo, S.T., M.T.', null, 'B', '137/SK/BAN-PT/Akred/Dpl-III/IV/2015', '2015-08-15', 'TRUS', 'SPRI', 'SPRI', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Industrial Technology', '89.0361.380.E', 'Electrical Engineering', 'Electrical Engineering', 't', 'elektro@akprind.ac.id', 'elektro.akprind.ac.id', 'S.T.');
INSERT INTO "public"."prodi" VALUES ('05', 'Teknologi Industri', 'Teknik Informatika', 'Teknik Informatika', 'Akreditasi', 'S-1', 'FTI', 'Uning Lestari, S.T., M.Kom.', null, 'B', '003/SK/BAN-PT/Akred/S/I/2014', '2014-01-09', 'TRUS', 'ULES', 'ULES', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Industrial Technology', '89.0361.380.E', 'Informatics Engineering', 'Informatics Engineering', 't', 'informatika@akprind.ac.id', 'informatika.akprind.ac.id', 'S.Kom.');
INSERT INTO "public"."prodi" VALUES ('06', 'Sains Terapan', 'Statistika', 'Statistika', 'Akreditasi', 'S-1', 'FST', 'Kris Suryowati, S.Si., M.Si.', null, 'B', '293/SK/BAN-PT/Akred/S/VIII/2014', '2014-08-23', 'NOER', 'KSUR', 'KSUR', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Applied Sciences', '83.1058.207.E', 'Mathematics', 'Statistics', 't', 'statistika@akprind.ac.id', 'statistika.akprind.ac.id', 'S.Si.');
INSERT INTO "public"."prodi" VALUES ('07', 'Sains Terapan', 'Sistem Komputer', 'Sistem Komputer', 'Akreditasi', 'S-1', 'FST', 'Dra. Harmastuti, M.Kom.', null, 'B', '447/SK/BAN-PT/Akred/S/XI/2014', '2014-11-15', 'NOER', 'HARM', 'HARM', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Applied Sciences', '83.1058.207.E', 'Mathematics', 'Computer Science', 't', 'siskom@akprind.ac.id', 'siskom.akprind.ac.id', 'S.Kom.');
INSERT INTO "public"."prodi" VALUES ('08', 'Sains Terapan', 'Fisika', 'Fisika', 'Terdaftar', 'S-1', 'MIPA', 'Dra. Kurnia, M.Sc.', null, null, '0606/O/1990', '1990-09-15', 'NOER', 'HARM', 'HARM', 'Menteri Pendidikan dan Kebudayaan', 'Faculty of Applied Sciences', '02.1262.569.E', 'Physic', 'Physic', 'f', null, null, null);
INSERT INTO "public"."prodi" VALUES ('09', 'Sains Terapan', 'Kimia', 'Ilmu Kimia', 'Terdaftar', 'S-1', 'MIPA', 'Dra. Noeryanti, M.Si.', null, null, '0606/O/1990', '1990-09-15', 'NOER', 'DIND', 'DIND', 'Menteri Pendidikan dan Kebudayaan', 'Faculty of Applied Sciences', '02.1262.569.E', 'Chemistry', 'Chemistry', 'f', null, null, null);
INSERT INTO "public"."prodi" VALUES ('10', 'Teknologi Mineral', 'Teknik Geologi', 'Teknik Geologi', 'Akreditasi', 'S-1', 'FTM', 'Danis Agoes Wiloso ,S.T., M.T.', null, 'B', '163/SK/BAN-PT/Ak-SURV/S/VI/2014', '2014-01-09', 'SMUL', 'DAWIL', 'DAWIL', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Mineral Technology', '16 0869 767 E', 'Geological Engineering', 'Geological Engineering', 't', 'geologi@akprind.ac.id', 'geologi.akprind.ac.id', 'S.T.');
INSERT INTO "public"."prodi" VALUES ('11', 'Sains Terapan', 'Teknik Lingkungan', 'Teknik Lingkungan', 'Akreditasi', 'S-1', 'FST', 'Purnawan, S.T., M.Eng.', null, 'B', '447/SK/BAN-PT/Akred/S/XI/2014', '2014-11-15', 'NOER', 'PRWN', 'PRWN', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Applied Sciences', '83.1058.207.E', 'Teknik Lingkungan', 'Teknik Lingkungan', 't', 'lingkungan@akprind.ac.id', 'lingkungan.akprind.ac.id', 'S.T.');
INSERT INTO "public"."prodi" VALUES ('31', 'Teknologi Industri', 'Teknik Kimia', 'Teknik Kimia', 'Akreditasi', 'D-3', 'FTI', 'Ir. Murni Yuniwati, M.T.', null, 'B', '005/BAN-PT/Ak-IV/Dpl-III/VI/2004', '2004-06-17', 'TRUS', 'MYUN', 'MYUN', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Industrial Technology', '89.0361.380.E', 'Chemical Engineering', 'Chemical Engineering', 'f', null, null, null);
INSERT INTO "public"."prodi" VALUES ('32', 'Teknologi Industri', 'Teknik Industri', 'Teknik Industri', 'Akreditasi', 'D-3', 'FTI', 'Endang Widuri Asih, S.T., M.T.', null, 'B', '362/SK/BAN-PT/Akred/Dpl-III/IX/2014', '2014-09-11', 'TRUS', 'EWID', 'ANDRE', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Industrial Technology', '89.0361.380.E', 'Management & Industrial Engineering', 'Management & Industrial Engineering', 't', 'teknikindustri@akprind.ac.id', 'industri.akprind.ac.id', 'A.Md.');
INSERT INTO "public"."prodi" VALUES ('33', 'Teknologi Industri', 'Teknik Mesin', 'Teknik Mesin', 'Akreditasi', 'D-3', 'FTI', 'Nidia Lestari, S.T., M.Eng.', null, 'B', '292/SK/BAN-PT/Akred/Dpl-III/VIII/2014', '2014-08-23', 'TRUS', 'NLES', 'IGUG', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Industrial Technology', '89.0361.380.E', 'Mechanical Engineering', 'Mechanical Engineering', 't', 'mesin@akprind.ac.id', 'mesin.akprind.ac.id', 'A.Md.');
INSERT INTO "public"."prodi" VALUES ('34', 'Teknologi Industri', 'Teknik Elektro', 'Teknik Elektronika', 'Akreditasi', 'D-3', 'FTI', 'Sigit Priyambodo, S.T., M.T.', null, 'B', '137/SK/BAN-PT/Akred/Dpl-III/IV/2015', '2015-04-06', 'TRUS', 'SPRI', 'BFIR', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Industrial Technology', '89.0361.380.E', 'Electrical Engineering', 'Electrical Engineering', 't', 'elektro@akprind.ac.id', 'elektro.akprind.ac.id', 'A.Md.');
INSERT INTO "public"."prodi" VALUES ('35', 'Teknologi Industri', 'Teknik Informatika', 'Manajemen Informatika', 'Akreditasi', 'D-3', 'FTI', 'Uning Lestari, S.T., M.Kom.', null, 'B', '380/SK/BAN-PT/Akred/Dpl-III/IX/2014', '2014-09-27', 'TRUS', 'ULES', 'EFAT', 'Badan Akreditasi Nasional Perguruan Tinggi', 'Faculty of Industrial Technology', '89.0361.380.E', 'Informatic Engineering', 'Informatic Management', 't', 'informatika@akprind.ac.id', 'informatika.akprind.ac.id', 'A.Md.');

-- ----------------------------
-- Primary Key structure for table prodi
-- ----------------------------
ALTER TABLE "public"."prodi" ADD PRIMARY KEY ("kode");
