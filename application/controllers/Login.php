<?php

	class Login extends CI_Controller
	{
		
		function __construct(){
			parent::__construct();
			$this->load->model('Crud');
		}
		private $master_tabel='user';
		private $id='user_id';

		function index(){
			if($this->session->userdata('login')==true AND $this->session->userdata('level')==1 ){
				redirect(site_url('dashboard/admin'));
			}elseif($this->session->userdata('login')==true AND $this->session->userdata('level')!=1 ){
				redirect(site_url('formulir/user'));
			}
			$this->load->view('template/login');
			//phpinfo();
			//redirect(site_url('dashboard/user'));
		}
		function daftar(){
			$this->load->view('daftar');
		}
		function aksi_login(){
			$username=$this->input->post('username');
			//$password=md5($this->input->post('password'));
			$password=$this->input->post('password');
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array('user_nim'=>$username),
				'where_'=>array('user_password'=>$password),
				//'or_where'=>array('user_email'=>$username)
			);
			$cek_user=$this->Crud->read($query);
			if($cek_user->num_rows()==1){
				$user=$cek_user->row();
				$dt_session=array(
						'user_id'=>$user->user_id,
						'nim'=>$user->user_nim,
						'level'=>$user->user_level,
						'login'=>true,
						);
				$this->session->set_userdata($dt_session);				
				if($this->session->userdata('level')==1){
				  redirect(site_url("dashboard/admin"));
				}else{
				  //redirect(site_url("dashboard/user"));	
					redirect(site_url("formulir/user"));	
				}
				//print_r($user);
			}else{
				$this->session->set_flashdata('error','username tidak ditemukan');
				redirect(base_url('Login'));
			}
		}
		function logout(){
			$this->session->sess_destroy();
			redirect(base_url('Login'));
		}	
	
	}
?>