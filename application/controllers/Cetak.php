<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent:: __construct();
		$this->load->model(array('Crud'));
		// if($this->session->userdata('login')!=true){
		// 	redirect(site_url('Login/logout'));
		// }
	}
	function cetak($data){
		$nama_dokumen=$data['judul']; //Beri nama file PDF hasil.
		require_once('./asset/third_party/mPDF/mpdf.php');
		$mpdf= new mPDF('c',array(210,297),'',10,20,20,30,20);	

		$mpdf->SetHTMLHeader('
		<div style="text-align: right; font-weight: bold;">
		    <img src="./asset/dist/img/avatar6.png" width="60px" height="60px">'.$nama_dokumen.'
		</div>');
		$mpdf->SetHTMLFooter('
		<table width="100%">
		    <tr>
		        <td width="33%">{DATE j-m-Y}</td>
		        <td width="33%" align="center">{PAGENO}/{nbpg}</td>
		        <td width="33%" style="text-align: right;">'.$nama_dokumen.'</td>
		    </tr>
		</table>');		
		$mpdf->AddPage('P');
		//$view=$this->load->view('laporan/admin/index','',true);
		$mpdf->WriteHTML($data['view']);
		$mpdf->Output($nama_dokumen.".pdf",'I');		
	}
	function index(){
		$judul='Laporan Bulanan';
		$view=$this->load->view('laporan/admin/index','',true);	
		$data=array(
			'judul'=>$judul,
			'view'=>$view,
			);
		$this->cetak($data);					
	}	
	// function index(){
	// 	$nama_dokumen='PDF'; //Beri nama file PDF hasil.
	// 	require_once('./asset/mpdf/src/Mpdf.php');

	// 	$mpdf = new Mpdf();
	// 	$mpdf->WriteHTML('<h1>Hello world!</h1>');
	// 	$mpdf->Output();						
	// }	
}