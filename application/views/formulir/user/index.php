<div id="view">
	<?php if(count($cek_nim)<>0):?>
		<div class="row">
			<div class="col-sm-2">
				<div class="form-group">					
						<a href="<?=site_url($global->url.'/cetakblangko')?>" target="_blank" class="btn btn-warning btn-flat btn-block"><span class="fa fa-print"></span> Cetak Formulir</a>
				</div>
			</div>
		</div>				
	<?php endif;?>
	<div class="row">
		<div class="col-sm-12">
            <?php if($this->session->flashdata('error')):?>	
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Informasi!</h4>
                	<?= $this->session->flashdata('error')?>
              </div>              				
			<?php endif;?>
	          <div class="alert alert-info alert-dismissible">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <h4><i class="icon fa fa-graduation-cap"></i> Informasi Wisuda</h4>
	            	<?= ucwords($thakademik->thakademik_keterangan)?>
	          </div> 				
			<div class="box box-solid">
				<?php if(count($cek_nim)<>0):?>
					<?php include 'edit.php';?>					
				<?php else:?>
					<?php include 'add.php';?>
				<?php endif;?>
			</div>			
		</div>
	</div>
</div>
<?php include 'action.js';?>
<script type="text/javascript">
	    setTimeout(function () {
        var url=$('#tabel').attr('url');
        $("#tabel").load(url);
        //alert(url);
    }, 200); 
</script>