<form method="POST" action="<?= base_url($global->url.'/update')?>" enctype="multipart/form-data">
	<div class="box-header with-border">
		<h3 class="box-title"><?= ucwords($global->headline)?></h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Id</label>
					<input type="text" required readonly class="form-control" name="id" title="Wajib diisi" value="<?= $formulir->formulir_id?>">
				</div>				
				<div class="form-group">
					<label>Periode/Tahun Akademik</label>
					<input type="text" required readonly class="form-control" name="formulir_thakademik" title="Wajib diisi" value="<?= $thakademik->thakademik_kode?>">
				</div>							
				<div class="form-group">
					<label>Nomor Mahasiswa</label>
					<input reauired type="text" name="formulir_nim" value="<?=$formulir->formulir_nim?>"  class="form-control" title="Wajib diisi">
				</div>
				<div class="form-group">
					<label>Jenjang/Jurusan/Fakultas</label>
					<select required class="selectdata form-control" style="width:100%" name="formulir_jurusan">
						<?php foreach($prodi AS $row):?>
							<option value="<?= ucwords($row->jenjang).'/'.ucwords($row->jurusan).'/'.ucwords($row->fakultas)?>" <?=$formulir->formulir_jurusan==ucwords($row->jenjang).'/'.ucwords($row->jurusan).'/'.ucwords($row->fakultas) ? 'selected':''?>><?= ucwords($row->jenjang).'/'.ucwords($row->jurusan).'/'.ucwords($row->fakultas)?></option>
						<?php endforeach;?>
					</select>
				</div>																						
				<div class="form-group">
					<label>Agama</label>
					<select required class="selectdata form-control" style="width:100%" name="formulir_agama" title="Wajib diisi">
						<?php foreach($agama AS $row):?>
							<option value="<?=$row->agama?>" <?=$formulir->formulir_agama==$row->agama ? 'selected':''?>><?=$row->agama?></option>
						<?php endforeach;?>
					</select>							
				</div>
				<div class="form-group">
					<label>Tahun Masuk/Lulus</label>
					<div class="row">
						<div class="col-sm-6">
							<input required type="text" name="formulir_tahunmasuk" placeholder="Tahun masuk" class="form-control" title="Wajib diisi" value="<?= $formulir->formulir_tahunmasuk?>">
						</div>
						<div class="col-sm-6">
							<input required type="text" name="formulir_tahunlulus" placeholder="Tahun lulus" class=" form-control" title="Wajib diisi" value="<?= $formulir->formulir_tahunlulus?>">
						</div>										
					</div>
				</div>	
				<div class="form-group">
					<label>Judul Skripsi</label>
					<textarea required rows="4" class="text-capitalize form-control" name="formulir_judulskripsi" title="Wajib disisi"><?= $formulir->formulir_judulskripsi?></textarea>
				</div>																																														
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>IPK</label>
					<input required type="text" name="formulir_ipk" class="form-control" title="Wajib diisi" value="<?= $formulir->formulir_ipk?>">
					<p class="help-block">Dalam Skala 4</p>
				</div>		
				<div class="form-group">
					<label>Dosen Pembingbing 1</label>
					<select class="selectdata form-control" style="width:100%" name="formulir_dosbim1">
						<?php foreach($dosen AS $row):?>
							<option value="<?= (!empty($row->gelar1) ? $row->gelar1.',':'').$row->nama.', '.$row->gelar2?>" <?=$formulir->formulir_dosbim1==(!empty($row->gelar1) ? $row->gelar1.',':'').$row->nama.', '.$row->gelar2 ? 'selected':''?>><?= (!empty($row->gelar1) ? $row->gelar1.',':'').$row->nama.', '.$row->gelar2?></option>
						<?php endforeach;?>
					</select>
				</div>	
				<div class="form-group">
					<label>Dosen Pembingbing 2</label>
					<select class="selectdata form-control" style="width:100%" name="formulir_dosbim2">
						<?php foreach($dosen AS $row):?>
							<option value="<?= (!empty($row->gelar1) ? $row->gelar1.',':'').$row->nama.', '.$row->gelar2?>" <?=$formulir->formulir_dosbim2==(!empty($row->gelar1) ? $row->gelar1.',':'').$row->nama.', '.$row->gelar2 ? 'selected':''?>><?= (!empty($row->gelar1) ? $row->gelar1.',':'').$row->nama.', '.$row->gelar2?></option>
						<?php endforeach;?>
					</select>
				</div>	
				<div class="form-group">
					<img src="<?= base_url('upload/foto/'.$formulir->formulir_foto)?>" width="150px" height="150px" cation="Foto"><br>
					<label>Foto</label>
					<input type="file" name="filefoto" title="Wajib dilampirkan">
					<p class="help-block">Maksimal ukuran 5mb, format JPG|jpeg</p>
					
				</div>	
				<div class="form-group">
					<img src="<?= base_url('upload/buktibayar/'.$formulir->formulir_buktibayar)?>" width="150px" height="150px" title="Bukti bayar"><br>
					<label>Bukti Pembayaran</label>
					<input type="file" name="filepembayaran" title="Wajib dilampirkan">
					<p class="help-block">Maksimal ukuran 5mb, format JPG|jpeg</p>
					
				</div>
																																																																																								
			</div>
		</div>
	</div>
	<div class="box-header with-border">
		<h3 class="box-title"><?= ucwords('data diri')?></h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>Nama</label>
					<input required type="text" name="formulir_nama"  class="text-capitalize form-control" title="Wajib diisi" value="<?= ucwords($formulir->formulir_nama)?>">
				</div>	
				<div class="form-group">
					<label>Tempat Lahir</label>
					<input required  type="text" name="formulir_tempatlahir" class="form-control" title="Wajib diisi" value="<?= ucwords($formulir->formulir_tempatlahir)?>">
				</div>	
				<div class="form-group">
					<label>Tanggal Lahir</label>
					<input required type="text" name="formulir_tgllahir"  value="<?= date('d-m-Y')?>" class="datepicker form-control" title="Wajib diisi" value="<?= date('d-m-Y',strtotime($formulir->formulir_tgllahir))?>">
				</div>																					
				<div class="form-group">
					<label>Nama Orang Tua/Wali</label>
					<input required type="text" name="formulir_namaorangtua" class="text-capitalize form-control" title="Wajib diisi" value="<?= ucwords($formulir->formulir_namaorangtua)?>">
				</div>
				<div class="form-group">
					<label>Alamat di Yogyakarta</label>
					<textarea required rows="4" class="text-capitalize form-control" name="formulir_alamatyogya" title="Wajib disisi"><?= $formulir->formulir_alamatyogya?></textarea>
				</div>								
				<div class="form-group">
					<label>Alamat asal</label>
					<textarea required rows="4" class="text-capitalize form-control" name="formulir_alamatasal" title="Wajib disisi"><?= $formulir->formulir_alamatasal?></textarea>
				</div>
				<div class="form-group">
					<label>Email</label>
					<input required type="email" name="formulir_email" class="form-control" title="Wajib diisi" value="<?= $formulir->formulir_email?>">
				</div>	
				<div class="form-group">
					<label>No. Telp</label>
					<input required type="text" name="formulir_notelp" class="form-control" title="Wajib diisi" value="<?= $formulir->formulir_notelp?>">
				</div>
				<div class="form-group">
					<label>Apabila sudah bekerja, kerja di</label>
					<input type="text" name="formulir_perusahaan" class="text-capitalize form-control" value="<?= $formulir->formulir_perusahaan?>">
				</div>	
				<div class="form-group">
					<label>Alamat Tempat Kerja</label>
					<textarea  rows="4" class="text-capitalize form-control" name="formulir_alamatkantor" title="Wajib disisi"><?= $formulir->formulir_alamatkantor?></textarea>
				</div>								
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<button type="submit" name="submit" value="submit" class="btn btn-flat btn-block btn-warning  ">Update</button>
				<a href="<?=site_url($global->url.'/cetakblangko')?>" target="_blank" class="hide btn btn-flat btn-block btn-warning"><span class="fa fa-print"></span> Cetak</a>
			</div>
		</div>
	</div>				
</form>	