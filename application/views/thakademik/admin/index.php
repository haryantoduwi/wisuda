<div id="view">
  <div class="row">
    <div class="col-sm-12">
      <div class="alert alert-info alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-check"></i> Informasi</h4>
            Mohon perhatian, hanya boleh satu periode yang aktif.
        </div>       
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">
      <div class="form-group">
        <button onclick="add();" id="add" url="<?= base_url($global->url.'add')?>" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah</button>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div id="tabel" url="<?= base_url($global->url.'tabel')?>">
        <div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
      </div>
    </div>    
  </div>
</div>
<?php include 'action.js';?>
<script type="text/javascript">
	    setTimeout(function () {
        var url=$('#tabel').attr('url');
        $("#tabel").load(url);
        //alert(url);
    }, 200); 
</script>